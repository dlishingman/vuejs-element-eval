# vuejs-element-eval

## Setup

### Prerequisites:
* node v8.11.4 or higher
* yarn v1.9.4 or higher
```
yarn install
```

### Run for development
```
yarn run serve
```
* Navigate to http://localhost:8080/
* Click on "Contact Info Dialog Test" to mimick the Investopedia "add symbol to watchlist" flow using the element.io dialog component.

### Compile and minifies for production
```
yarn run build
```

### Lint and fix files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
