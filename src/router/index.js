import Vue from 'vue';
import Router from 'vue-router';
import HelloWorld from '@/pages/HelloWorld';
import DialogTest from '@/pages/DialogTest';
import ContactInfo from '@/pages/ContactInfo';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: HelloWorld.name,
      component: HelloWorld,
    },
    {
      path: '/dialog-test',
      name: DialogTest.name,
      component: DialogTest,
    },
    {
      path: '/contact-info-dialog-test',
      name: ContactInfo.name,
      component: ContactInfo,
    },
  ]
});
